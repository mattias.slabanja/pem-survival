

functions {
#include "general_utilities_functions.stan"
}


data {
    int<lower=0>                        n_obs;
    array[n_obs] real<lower=0>          obs_duration;
    array[n_obs] int<lower=0, upper=1>  obs_right_censored;
    array[n_obs] real<lower=0>          obs_truncation;
    array[n_obs] int<lower=0, upper=1>  obs_left_censored;

    real<lower=0>                       prior_rate;
}

transformed data {
    int                     n_left_cens =       number_of_nonzeroes(obs_left_censored);
    array[n_obs] int        left_cens_ix =      inverse_nonzero_indices(obs_left_censored);
    array[n_left_cens] int  left_cens_obs_ix =  nonzero_indices(obs_left_censored);
    
    vector[n_left_cens] left_cens_duration = to_vector(obs_truncation[left_cens_obs_ix]);
}

parameters {
    real<lower=0> base_hazard;
    vector<lower=0>[n_left_cens] left_cens_impute;
}

model {
    base_hazard         ~ gamma(2.0, 1.0/prior_rate);

    left_cens_impute + left_cens_duration ~ exponential(prior_rate);

    for (obs_ix in 1:n_obs) {
        real duration = obs_duration[obs_ix];
        real truncated = 0.0;
        
        if (obs_left_censored[obs_ix]) {
            duration += left_cens_impute[left_cens_ix[obs_ix]];
            truncated = left_cens_impute[left_cens_ix[obs_ix]];
        }
        
        if (obs_truncation[obs_ix] > 0.0) {
            truncated = obs_truncation[obs_ix];
        }

        if (obs_right_censored[obs_ix]) {
            target += exponential_lccdf(duration | base_hazard);
            target += -exponential_lccdf(truncated | base_hazard);
        }
        else {
            duration ~ exponential(base_hazard) T[truncated, ];
        }
    }

}

