

functions {
#include "general_utilities_functions.stan"
}


data {
    int<lower=0>                        n_obs;
    array[n_obs] real<lower=0>          obs_duration;
    array[n_obs] int<lower=0, upper=1>  obs_right_censored;
    array[n_obs] real<lower=0>          obs_truncation;
    array[n_obs] int<lower=0, upper=1>  obs_left_censored;
    
    int<lower=0>                        n_vars;
    array[n_obs] vector[n_vars]         obs_vars;

    real<lower=0>                       prior_rate;
}

transformed data {
    int                     n_left_cens =       number_of_nonzeroes(obs_left_censored);
    array[n_obs] int        left_cens_ix =      inverse_nonzero_indices(obs_left_censored);
    array[n_left_cens] int  left_cens_obs_ix =  nonzero_indices(obs_left_censored);
    
    vector[n_left_cens] left_cens_duration = to_vector(obs_truncation[left_cens_obs_ix]);
}

parameters {
    real log_base_hazard;
    vector[n_vars] beta;
    vector<lower=0>[n_left_cens] left_cens_impute;
}

model {
    exp(log_base_hazard) ~ gamma(2.0, 1.0/prior_rate);
    beta ~ normal(0, 1);

    left_cens_impute + left_cens_duration ~ exponential(prior_rate);

    for (obs_ix in 1:n_obs) {
        real duration = obs_duration[obs_ix];
        real truncated = 0.0;
        real log_hazard = log_base_hazard;
        
        if (obs_left_censored[obs_ix]) {
            duration += left_cens_impute[left_cens_ix[obs_ix]];
            truncated = left_cens_impute[left_cens_ix[obs_ix]];
        }
        
        if (obs_truncation[obs_ix] > 0.0) {
            truncated = obs_truncation[obs_ix];
        }
        
        log_hazard += dot_product(beta, obs_vars[obs_ix]);

        if (obs_right_censored[obs_ix]) {
            target += exponential_lccdf(duration | exp(log_hazard));
            target += -exponential_lccdf(truncated | exp(log_hazard));
        }
        else {
            duration ~ exponential(exp(log_hazard)) T[truncated, ];
        }
    }
}

generated quantities {
    real base_hazard = exp(log_base_hazard);
}

