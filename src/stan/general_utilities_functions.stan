  
int number_of_nonzeroes(array [] int a) {
  int result = 0;
  for (ix in 1:size(a)) {
    if (a[ix] != 0) {
      result += 1;
    }
  }
  return result;
}

int number_of_nonzeroes(array [] real a, real tolerance) {
  int result = 0;
  for (ix in 1:size(a)) {
    if (abs(a[ix]) >= tolerance) {
      result += 1;
    }
  }
  return result;
}

array[] int nonzero_indices(array[] int a) {
  int n_nonzero = number_of_nonzeroes(a);
  int nonzero_ix = 1;
  array[n_nonzero] int result;
  for (ix in 1:size(a)) {
    if (a[ix] != 0) {
      result[nonzero_ix] = ix;
      nonzero_ix += 1;
    }
  }
  return result;
}

array[] int inverse_nonzero_indices(array[] int a) {
  int nonzero_ix = 1;
  array[size(a)] int result = rep_array(0, size(a));
  for (ix in 1:size(a)) {
    if (a[ix] != 0) {
      result[ix] = nonzero_ix;
      nonzero_ix += 1;
    }
  }
  return result;
}

array[] int nonzero_indices(array[] real a, real tolerance) {
  int n_nonzero = number_of_nonzeroes(a, tolerance);
  int nonzero_ix = 1;
  array[n_nonzero] int result;
  for (ix in 1:size(a)) {
    if (a[ix] != 0) {
      result[nonzero_ix] = ix;
      nonzero_ix += 1;
    }
  }
  return result;
}

array[] int inverse_nonzero_indices(array[] real a, real tolerance) {
  int nonzero_ix = 1;
  array[size(a)] int result = rep_array(0, size(a));
  for (ix in 1:size(a)) {
    if (abs(a[ix]) >= tolerance) {
      result[ix] = nonzero_ix;
      nonzero_ix += 1;
    }
  }
  return result;
}

