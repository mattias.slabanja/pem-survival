
array[] int duration_ix_to_pem_period_ix(array[] int pem_period_end) {
    int pem_periods = size(pem_period_end);
    int end_duration = pem_period_end[pem_periods];
    array[end_duration] int result;

    for (dur_ix in 1:pem_period_end[1]) {
        result[dur_ix] = 1;
    }
    for (pem_ix in 2:pem_periods) {
        for (dur_ix in (pem_period_end[pem_ix-1]+1):pem_period_end[pem_ix]) {
            result[dur_ix] = pem_ix;
        }
    }
    return result;
}

real observed_duration_lpmf(int event, 
                            int duration, 
                            vector base_hazard,
                            array[] int pem_period_ix) {
    real result = 0.0;
    int pem_ix;
    
    if (duration > 0) {
      for (time_ix in 1:(duration - 1)) {
          pem_ix = pem_period_ix[time_ix];
          result += poisson_lupmf(0 | base_hazard[pem_ix]);
      }
      pem_ix = pem_period_ix[duration];
      result += poisson_lupmf(event | base_hazard[pem_ix]);
    }
    return result;
}

vector cumulative_average(vector haz) {
    int n = size(haz);
    vector[n] result = cumulative_sum(haz);
    for (ix in 2:n) {
        result[ix] *= 1.0 / ix;
    }
    return result;
}

vector expected_unit_period(vector period_survival) {
    int n = size(period_survival);
    vector[n] result = (1.0 - period_survival);
    real f = 1.0;
    
    for (ix in 2:(n-1)) {
        f *= period_survival[ix-1];
        result[ix] *= f;
    }
    result[n] = 1.0 - sum(result[1:(n-1)]);

    return result;
}

real calculate_effective_hazard(vector haz, vector period_survival) {
    return sum(cumulative_average(haz) .* expected_unit_period(period_survival));
}

real censored_tail_lpdf(real censored_time,
                        int start_time_ix, 
                        vector base_hazard,
                        vector period_survival,
                        array[] int pem_period_ix) {
    int time_ix = start_time_ix;
    int end_time_ix = size(pem_period_ix);

    real expected_effective_hazard = calculate_effective_hazard(base_hazard[pem_period_ix[time_ix:end_time_ix]], 
                                                                period_survival[pem_period_ix[time_ix:end_time_ix]]);

    return exponential_lupdf(censored_time | expected_effective_hazard);
}


real truncated_observations_lpdf(real hidden_pseudo_count,
                                 int truncation_length, 
                                 vector base_hazard,
                                 array[] int pem_period_ix) {
    real result = 0.0;

    real pseudo_count = 1.0 + hidden_pseudo_count;
    real factor = pow(pseudo_count, -1.0 / truncation_length);
    for (time_ix in 1:truncation_length) {
        real next_pseudo_count = pseudo_count * factor;
        real pseudo_events = pseudo_count - next_pseudo_count;

        int pem_ix = pem_period_ix[time_ix];
        result += beta_lupdf(base_hazard[pem_ix] | 1.0 + pseudo_events, 1.0 + next_pseudo_count);
        result -= log(1.0 + pseudo_count);

        pseudo_count = next_pseudo_count;
    }

    return result;
}
