
array[] int duration_ix_to_pem_period_ix(array[] int pem_period_end) {
    int pem_periods = size(pem_period_end);
    int end_duration = pem_period_end[pem_periods];
    array[end_duration] int result;

    for (dur_ix in 1:pem_period_end[1]) {
        result[dur_ix] = 1;
    }
    for (pem_ix in 2:pem_periods) {
        for (dur_ix in (pem_period_end[pem_ix-1]+1):pem_period_end[pem_ix]) {
            result[dur_ix] = pem_ix;
        }
    }
    return result;
}




real pem1_lccdf(real duration, 
                vector log_base_hazard, 
                real log_hazard, 
                array[] real pem_period_end,
                real lower_truncated) {
    real lccdf = 0.0;
    real pem_period_start = 0.0;
    int pem_ix = 1;
    int max_pem_ix = size(pem_period_end);

    if (duration < 0.0
        || duration < lower_truncated 
        || duration > pem_period_end[max_pem_ix]) {
        return 0.0;
    }

    if (lower_truncated > 0.0) {
        while (lower_truncated > pem_period_end[pem_ix]) {
            pem_period_start = pem_period_end[pem_ix];
            pem_ix += 1;
        }
        if (duration > pem_period_end[pem_ix]) {
            lccdf = -exp(log_base_hazard[pem_ix] + log_hazard) * (pem_period_end[pem_ix] - lower_truncated);
            pem_period_start = pem_period_end[pem_ix];
            pem_ix += 1;
        }
        else {
            pem_period_start = lower_truncated;
        }
    }

    while (duration > pem_period_end[pem_ix]) {
        lccdf += -exp(log_base_hazard[pem_ix] + log_hazard) * (pem_period_end[pem_ix] - pem_period_start);
        pem_period_start = pem_period_end[pem_ix];
        pem_ix += 1;
    }
    lccdf += -exp(log_base_hazard[pem_ix] + log_hazard) * (duration - pem_period_start);

    return lccdf;
}


real pem1_lcdf(real duration, 
                vector log_base_hazard, 
                real log_hazard, 
                array[] real pem_period_end,
                real lower_truncated) {
    return log_diff_exp(0,
                        pem1_lccdf(duration |
                                  log_base_hazard,
                                  log_hazard,
                                  pem_period_end,
                                  lower_truncated));
}


real pem1_lpdf(real duration, 
               vector log_base_hazard, 
               real log_hazard, 
               array[] real pem_period_end,
               real lower_truncated) {

    real lpdf = 0.0;
    real pre_lccdf = 0.0;
    real interval_lccdf = 0.0;
    real pem_period_start = 0.0;
    int pem_ix = 1;
    int max_pem_ix = size(pem_period_end);

    if (duration < 0.0
        || duration < lower_truncated 
        || duration > pem_period_end[max_pem_ix]) {
        return negative_infinity();
    }

    if (lower_truncated > 0.0) {
        while (lower_truncated > pem_period_end[pem_ix]) {
            pem_period_start = pem_period_end[pem_ix];
            pem_ix += 1;
        }

        if (duration > pem_period_end[pem_ix]) {
            pre_lccdf += exponential_lccdf(pem_period_end[pem_ix] - lower_truncated | exp(log_base_hazard[pem_ix] + log_hazard));
            pem_period_start = pem_period_end[pem_ix];
            pem_ix += 1;
        }
        else {
            pem_period_start = lower_truncated;
        }
    }

    while (duration > pem_period_end[pem_ix]) {
        pre_lccdf += exponential_lccdf(pem_period_end[pem_ix] - pem_period_start | exp(log_base_hazard[pem_ix] + log_hazard));
        pem_period_start = pem_period_end[pem_ix];
        pem_ix += 1;
    }
    interval_lccdf = exponential_lccdf(pem_period_end[pem_ix] - pem_period_start | exp(log_base_hazard[pem_ix] + log_hazard));    
    lpdf = exponential_lupdf(duration - pem_period_start | exp(log_base_hazard[pem_ix] + log_hazard));
    return lpdf + log_diff_exp(pre_lccdf, pre_lccdf + interval_lccdf) - log_diff_exp(0.0, interval_lccdf);

}

