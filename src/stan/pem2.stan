

functions {
#include "general_utilities_functions.stan"
#include "pem2_functions.stan"
}



data {
    int<lower=1>                                   n_locations;
    int<lower=0>                                   n_obs;

    array[n_obs] real<lower=0>                     obs_duration;
    array[n_obs] int<lower=0, upper=1>             obs_right_censored;
    array[n_obs] real<lower=0>                     obs_truncation;
    array[n_obs] int<lower=0, upper=1>             obs_left_censored;
    array[n_obs] int<lower=1, upper=n_locations>   obs_location;
    array[n_obs] real<lower=0>                     obs_start_time;

    int<lower=0>                        n_vars;
    array[n_obs] vector[n_vars]         obs_vars;

    int<lower=1>                        n_pem_periods;
    array[n_pem_periods] real<lower=0>  pem_period_end;

    int<lower=1>                        n_time_data;
    matrix[n_time_data, n_locations]    unemployment_std;

    real<lower=0>                       prior_rate;
}


transformed data {
    int                     n_left_cens =       number_of_nonzeroes(obs_left_censored);
    array[n_obs] int        left_cens_ix =      inverse_nonzero_indices(obs_left_censored);
    array[n_left_cens] int  left_cens_obs_ix =  nonzero_indices(obs_left_censored);
    
    vector[n_left_cens] left_cens_duration = to_vector(obs_truncation[left_cens_obs_ix]);

    array[n_obs] vector[n_pem_periods] average_unemployment = 
        calculate_pem_average_unemployment(obs_start_time, 
                                           obs_location, 
                                           pem_period_end, 
                                           unemployment_std);
    }

parameters {
    vector[n_pem_periods]  log_base_hazard;
    vector[n_vars] beta;
    real unemployment_param;
    vector<lower=0>[n_left_cens] left_cens_impute;
}

model {
    vector[n_pem_periods] log_unemp_hazard;

    exp(log_base_hazard) ~ gamma(2.0, 1.0/prior_rate);
    beta ~ normal(0, 1);
    unemployment_param ~ normal(0, 1);
    // truncate lower at left_cens_duration ?
    left_cens_impute + left_cens_duration ~ exponential(prior_rate);

    for (obs_ix in 1:n_obs) {
        real duration = obs_duration[obs_ix];
        real truncated = 0.0;
        real log_hazard = dot_product(beta, obs_vars[obs_ix]);

        log_unemp_hazard = unemployment_param * average_unemployment[obs_ix];

        if (obs_left_censored[obs_ix]) {
            duration += left_cens_impute[left_cens_ix[obs_ix]];
            truncated = left_cens_impute[left_cens_ix[obs_ix]];
        }
        else if (obs_truncation[obs_ix] > 0.0) {
            truncated = obs_truncation[obs_ix];
        }
        
        if (obs_right_censored[obs_ix]) {
            target += pem2_lccdf(duration | log_base_hazard, log_unemp_hazard, log_hazard, pem_period_end, truncated);
        }
        else {  
            target += pem2_lpdf(duration | log_base_hazard, log_unemp_hazard, log_hazard, pem_period_end, truncated);
        }
    }
}

generated quantities {
    vector[n_pem_periods] base_hazard = exp(log_base_hazard);
}

