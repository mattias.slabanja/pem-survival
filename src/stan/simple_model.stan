
functions {
#include "general_utilities_functions.stan"
#include "model_utilities_functions.stan"
}

data {
    int<lower=0>                        n_obs;
    array[n_obs] int<lower=1>           obs_duration;
    array[n_obs] int<lower=0, upper=1>  obs_event;
    array[n_obs] int<lower=0, upper=1>  obs_right_censored;
    array[n_obs] int<lower=0>           obs_truncation;

    real<lower=0>                       prior_rate;

    int<lower=1>                        n_pem_periods;
    array[n_pem_periods] int<lower=1>   pem_period_end;

    // time data ...
}

transformed data {
    int                     end_dur_ix =        pem_period_end[n_pem_periods];
    array[end_dur_ix] int   pem_period_ix =     duration_ix_to_pem_period_ix(pem_period_end);

    int                     n_right_cens =      number_of_nonzeroes(obs_right_censored);
    array[n_obs] int        right_cens_ix =     inverse_nonzero_indices(obs_right_censored);
    array[n_right_cens] int right_cens_obs_ix = nonzero_indices(obs_right_censored);

    int                     n_truncated =       number_of_nonzeroes(obs_truncation);
    array[n_obs] int        truncation_ix =     inverse_nonzero_indices(obs_truncation);
    array[n_truncated] int  truncated_obs_ix =  nonzero_indices(obs_truncation);
    vector[n_truncated]     truncation_length = to_vector(obs_truncation[truncated_obs_ix]);
}

parameters {
    vector<lower=0>[n_pem_periods]  base_hazard;
    vector<lower=0>[n_right_cens]   right_censored;
    vector<lower=0>[n_truncated]    hidden_observations;
}

model {
    base_hazard         ~ exponential(1.0 / prior_rate);
    right_censored      ~ exponential(prior_rate);
    hidden_observations ~ exponential(truncation_length / prior_rate);
    
    for (obs_ix in 1:n_obs) {
        target += observed_duration_lpmf(obs_event[obs_ix] |
                                         obs_duration[obs_ix],
                                         base_hazard,
                                         pem_period_ix);
    }

    for (rc_ix in 1:n_right_cens) {
        int obs_ix = right_cens_obs_ix[rc_ix];
        target += censored_tail_lpdf(right_censored[rc_ix] |
                                     obs_duration[obs_ix] + 1,
                                     base_hazard,
                                     pem_period_ix);
    }

    for (tr_ix in 1:n_truncated) {
        int obs_ix = truncated_obs_ix[tr_ix];
        target += truncated_observations_lpdf(hidden_observations[tr_ix] |
                                              obs_truncation[obs_ix],
                                              base_hazard,
                                              pem_period_ix);
    }

}

