
functions {
#include "general_utilities_functions.stan"
#include "model_utilities_functions.stan"

vector calc_all_effective_hazards(int n_right_cens, 
                                  array[] int right_cens_obs_ix,
                                  array[] int obs_duration, 
                                  array[] int pem_period_ix, 
                                  vector base_hazard, 
                                  vector expected_period_surv) {
    vector[n_right_cens] result;
    int end_time_ix = size(pem_period_ix);
    for (ix in 1:n_right_cens) {
       int obs_ix = right_cens_obs_ix[ix];
       int time_ix = obs_duration[obs_ix] + 1;
       result[ix] = calculate_effective_hazard(base_hazard[pem_period_ix[time_ix:end_time_ix]], 
                                               expected_period_surv[pem_period_ix[time_ix:end_time_ix]]);

    }
    return result;
}
}

data {
    int<lower=0>                        n_obs;
    array[n_obs] int<lower=0>           obs_duration;
    array[n_obs] int<lower=0, upper=1>  obs_event;
    array[n_obs] int<lower=0, upper=1>  obs_right_censored;
    array[n_obs] int<lower=0>           obs_truncation;

    real<lower=0>                       prior_rate;

    int<lower=1>                        n_pem_periods;
    array[n_pem_periods] int<lower=1>   pem_period_end;

    // time data ...
}

transformed data {
    int                     end_dur_ix =        pem_period_end[n_pem_periods];
    array[end_dur_ix] int   pem_period_ix =     duration_ix_to_pem_period_ix(pem_period_end);

    int                     n_right_cens =      number_of_nonzeroes(obs_right_censored);
    array[n_obs] int        right_cens_ix =     inverse_nonzero_indices(obs_right_censored);
    array[n_right_cens] int right_cens_obs_ix = nonzero_indices(obs_right_censored);

    int                     n_truncated =       number_of_nonzeroes(obs_truncation);
    array[n_obs] int        truncation_ix =     inverse_nonzero_indices(obs_truncation);
    array[n_truncated] int  truncated_obs_ix =  nonzero_indices(obs_truncation);
    vector[n_truncated]     truncation_length = to_vector(obs_truncation[truncated_obs_ix]);
}

parameters {
    vector<lower=0, upper=1>[n_pem_periods]  base_hazard;
    vector<lower=0>[n_right_cens]            right_censored;

    real<lower=0>  right_censor_rate;
}

transformed parameters {
   vector[n_pem_periods] expected_period_surv = exp(-base_hazard);
   vector[n_right_cens] effective_hazard = calc_all_effective_hazards(n_right_cens, 
                                                                      right_cens_obs_ix,
                                                                      obs_duration, 
                                                                      pem_period_ix, 
                                                                      base_hazard, 
                                                                      expected_period_surv);
 }

model {
    base_hazard         ~ beta(1.0 + 1.0, 
                               1.0 + (1.0 / prior_rate - 1.0));
    right_censor_rate   ~ lognormal(log(prior_rate), 1);
    right_censored      ~ exponential(right_censor_rate);
//    right_censored      ~ exponential(prior_rate);
    
    for (obs_ix in 1:n_obs) {
        target += observed_duration_lpmf(obs_event[obs_ix] |
                                         obs_duration[obs_ix],
                                         base_hazard,
                                         pem_period_ix);
    }

/*     for (rc_ix in 1:n_right_cens) {
        target += exponential_lupdf(right_censored[rc_ix] | effective_hazard[rc_ix]);
    }
 */
}

