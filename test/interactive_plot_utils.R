
library(cmdstanr)
library(bayesplot)
library(dplyr)
library(ggplot2)


get_parameter_as_long_df <- function(f, parameter, selection=NULL){
  df <- f$draws(parameter, format="df") %>% select(starts_with(parameter))

  colnames(df) <- stringr::str_replace(colnames(df), '[^\\[]+', 'ix')
  if (is.null(selection))
    column_names <- colnames(df)
  else
    column_names <- colnames(df)[selection]
  
  df %>%
    tidyr::gather(key="ix", value="value") %>%
    filter(ix %in% column_names) %>%
    mutate(ix = factor(ix, levels=column_names))
}

plot_hazard <- function(f, selection=NULL, xlab = "ix", ylab = "value") {
  get_parameter_as_long_df(f, "base_hazard", selection) %>%
    ggplot(aes(y=value, x=ix, fill=ix)) + 
    geom_violin(adjust=1/10) +
    xlab(xlab) + ylab(ylab)
}

plot_hazard_compare <- function(f_left, f_right, left_label="left", right_label="right") {
  bind_rows(get_parameter_as_long_df(f_left, "base_hazard") %>% mutate(label=left_label),
            get_parameter_as_long_df(f_right, "base_hazard") %>% mutate(label=right_label)) %>%
    ggplot(aes(y=value, x=ix, fill=label)) + 
    introdataviz::geom_split_violin(adjust=1/10)
}


# truncate_data <- function(data, start.time.range=5, truncate.to=0) {
#   start.time <- sample(1:start.time.range, data$n_observations, replace=TRUE)
#   end.time <- start.time + data$observation_length - 1
#   
#   keep.row <- end.time > truncate.to
#   data$observation_length <- data$observation_length[keep.row]
#   data$observation_event <- data$observation_event[keep.row]
#   data$right_censored <- data$right_censored[keep.row]
#   data$n_observations <- length(data$observation_length)
#   
#   start.time <- start.time[keep.row]
#   units.truncated <- truncate.to - start.time + 1
#   left_truncated <- ifelse(units.truncated > 0, units.truncated, 0)
#   data$left_truncated <- left_truncated
#   
#   data
# }


plot_data <- function(d, lwd=2) {
  n <- d$n_obs
  if ("obs_truncation" %in% names(d)) {
    truncation <- d$obs_truncation
  } 
  else {
    truncation <- rep(0, n)
  }
  
  known <- d$obs_duration - truncation
  unknown <- -truncation
  event <- d$obs_event
  
  max_pos <- max(known)
  min_pos <- min(unknown)
  p <- plot(NULL, ylim=c(0, 1), xlim=c(min_pos, max_pos))
  gray <- rgb(0.3, 0.3, 0.3, 0.3)
  blue <- rgb(0.0, 0.0, 0.6, 0.3)
  for (ix in 1:n) {
    y <- runif(1)
    x <- rnorm(1, 0, 0.1)
    lines(c(unknown[ix], 0) + x, c(y,y), col=gray, lwd=lwd)
    lines(c(0, known[ix]) + x, c(y,y), col=blue, lwd=lwd)
    if (event[ix] == 1) {
      points(c(known[ix]) + x, c(y), col=blue, pch=20, lwd=lwd)
    }
  }
  p
}


plot_estimated_S <- function(f, col = rgb(0, 0, 1, 0.01)) {
  m <- f$draws("base_hazard", format="matrix")
  log_p <- cbind(matrix(0, nrow=nrow(m), ncol=1),
                 pexp(1, rate=m, lower.tail = FALSE, log.p=TRUE))
  S <- apply(log_p, 1, cumsum) %>% exp %>% t
  
  p <- plot(NULL, xlim=c(0,ncol(m)), ylim=c(0,1))
  x <- 0:ncol(m)
  
  for (i in 1:nrow(S))
    lines(x, S[i,], col=col)
  
  p
}




