source("test/expose_cmdstanr_functions.R")
source("test/utilities.R")

assert <- assertthat::assert_that

general_functions <- expose_cmdstanr_functions(create_stan_file("src/stan/general_utilities_functions.stan"))


assert(general_functions$number_of_nonzeroes(c(0,0,0))
       == 0)
assert(general_functions$number_of_nonzeroes(c(0,1,0))
       == 1)
assert(general_functions$number_of_nonzeroes(c(1,1,1))
       == 3)

assert(length(general_functions$nonzero_indices(c(0,0,0))) 
       == 0)
assert(general_functions$nonzero_indices(c(0,0,1)) 
       == c(3))
assert(general_functions$nonzero_indices(c(0,1,0)) 
       == c(2))
assert(general_functions$nonzero_indices(c(1,0,0)) 
       == c(1))
assert(all(general_functions$nonzero_indices(c(1,0,1)) 
           == c(1,3)))
assert(all(general_functions$nonzero_indices(c(1,1,1)) 
           == c(1,2,3)))

assert(all(general_functions$inverse_nonzero_indices(c(0,0,0))
           == c(0,0,0)))
assert(all(general_functions$inverse_nonzero_indices(c(0,0,1))
           == c(0,0,1)))
assert(all(general_functions$inverse_nonzero_indices(c(0,1,0))
           == c(0,1,0)))
assert(all(general_functions$inverse_nonzero_indices(c(1,0,0))
           == c(1,0,0)))
assert(all(general_functions$inverse_nonzero_indices(c(1,0,1))
           == c(1,0,2)))
assert(all(general_functions$inverse_nonzero_indices(c(1,1,1))
           == c(1,2,3)))

